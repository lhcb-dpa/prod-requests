# Data Production Requests

This repository can be used to create DPA data production requests, i.e.:
- Concurrent or end-of-year Sprucing productions.
- Stripping or re-Stripping campaigns.

## Current production list and progress

The current list of productions is [available here](https://gitlab.cern.ch/lhcb-dpa/prod-requests/-/blob/nraja_prodProds_1/doc/production.md).

Plots of *number of files* or *luminosity* versus *time* are available from the Computing
[Processing passes of Re-stripping cycles](https://lhcbproject.web.cern.ch/lhcbproject/lbdirac/Reprocessing/).
Note that requests for plots for new processing campaigns need to be sent to the Computing project leader.

## An example following a full production creation

### Work on lxplus.
This should be done by someone / a group of persons who together have the following LHCbDIRAC roles : lhcb_user, lhcb_prmgr, lhcb_tech, lhcb_ppg

### Get LHCb proxy as user

On the lxplus command line
```bash
lhcb-proxy-init
```

### Submit the yaml file
```bash
lb-dirac dirac-production-request-submit --submit  --create-filetypes ./processing.yaml 
```

#### Notes :
- Author should be whoever is submitting (nraja in my case)
- The following strings (among others) define the bookkeeping path. So, check their definitions.
  - "configVersion", "conditions_description", "inProPass", "processing_pass", "event_type"

#### Example Output : 
```bash
-bash-4.2$ lb-dirac dirac-production-request-submit --submit  --create-filetypes ./processing.yaml 
Considering production 1 of 1: HLT2 Reprocessing (2022 MagDown)
Step 1 of 2: Created step with ID step.id=169996
Step 2 of 2: Created step with ID step.id=169997
Submitted production 116875 with sub productions []
```

### Look in the production request manager for 116875
- First sign it as lhcb_tech
- Then sign it as lhcb_ppg

### Submit the production as lhcb_prmgr
- RecoStripping + Merge (or another template e.g. Sprucing, as needed)
  - Note : For Run3 the modules are being re-written. To be updated.
- Within the template :
  - Validation (if needed)
  - Choose Run range wisely (if needed)

### Keep an eye on the productions using the DIRAC portal. In the above example, one can start with the "Request ID" 116875 in the Transformation Monitor and proceed from there.

- Keep a track of the application errors and report them in the relevant place.
- The last few jobs will need extra care to be run to completion.
- Jobs which have failed will be automatically rerun, unless it is a pathological failure, which will have to be dealt with as the case may be.

### At the end, one must "complete" and "clean" the production 


## Dealing with issues

Job failures found by the production team will be reported as issues in this repository.
The **template issue APPLICATION_BUG_REPORT** is to be chosen in this instance when creating the issue;
it is available via the drop-down menu *Description* during the creation of a new issue.

Bug reports will then be taken on board by the DPA Sprucing operation managers,
who will create (linked) scoped issues in the software repositories (e.g. Moore, DaVinci)
for experts to deal with.

## Workflow

Issues should always contain a `state` label to help keep track of what the current status is. These are manually managed and transitions between states should be made by whoever is responsible for the previous state. Some states are optional and only apply to some productions.

For each state the responsible part can be identified by the label's color:

* `#ed9121` DPA
* `#8fbc8f` production manager
* `#808080` data manager

The states are as follows:

1. prod-requests~"state::staging" (data manager, optional) Productions which are to be ran over data that is only stored on tape should. This label should be added by whoever first opens the issue after discussing with the OPG.
2. prod-requests~"state::preparing" (DPA) The configuration is being prepared and locally tested by the requestor.
3. prod-requests~"state::run-validation" (production manager) A small subset of runs will be processed by the computing team to ensure the configuation works on the grid.
4. prod-requests~"state::check-validation" (DPA) The requestor should now check the output of the validation production is as expected.
6. prod-requests~"state::ready" (production manager) The validation sample has been checked and the fully production should now be submitted. When processing the current year's data, DPA should explicitly state if the state in the merge request if the state after submission should be prod-requests~"state::running-concurrent" or prod-requests~"state::running".
5. prod-requests~"state::running-concurrent" (DPA, optional) The production is being ran concurrently to data taking and will stay in this state until all data has been exported from point 8, registered in LHCbDIRAC and data quality flags have been applied.
6. prod-requests~"state::update-end-run" (production manager DPA has requested that end run of the production be updated. After doing this the issue should return to prod-requests~"state::running-concurrent".
7. prod-requests~"state::running" (production manager) The production is running.
8. prod-requests~"state::debugging" (DPA, optional) An issue has been found with some files and needs to be investigated by the requestor. After doing this the issue should return to prod-requests~"state::running-concurrent" or prod-requests~"state::running".
9. prod-requests~"state::checking" (production manager, data manager) The production has been fully processed but various checks still need to be applied for issues like doubly processed data.
10. prod-requests~"state::done" The production is complete and the issue can be closed.