# Sprucing production request

### Summary

[Summarise the request(s), e.g. "Passthrough Sprucing for Runs ...]


[Give `start_run` and `end_run`]

```yaml
start_run: ...
end_run: ...
concurrent: yes or no
```

### Final task list 

- [ ] Ping Chris Burr on this issue and ask to end related AnaProds when production is COMPLETE and closing this issue

### (Annotated) YAML(s)

- The following template is for a single type of production.
- Most fields are pre-filled with common or default values.
- Fields marked as {...} are required a value.
- Various fields contain helper comments (starting with "#").
- Last but not least - once finalised, the YAML(s) need(s) to be validated!

```yaml
- name: {NAME}  # E.g. "TURBO passthrough Sprucing s24c1"
  comment: {COMMENT}  # E.g. "TURBO passthrough Sprucing s24c1 - first COLLISION24 data MagDown"
  type: Sprucing

  wg: DPA
  inform:  # Feel free to add any other relevant person
  - nskidmor
  - nraja
  priority: 1a

  input_dataset:
    event_type: {EVENT_TYPE} # 94000000 / 90000000 / 95100000 for turbo / full / turcal
    conditions_description: {CONDITIONS_DESCRIPTION} # E.g. Beam6800GeV-VeloClosed-MagDown-Excl-UT
    conditions_dict:
      configName: LHCb
      configVersion: {CONFIG_VERSION}  # E.g. "Collision24"
      inFileType: RAW
      inProPass: Real Data
      inDataQualityFlag: UNCHECKED, OK, BAD  # OPG decision from Autumn 2023
      inProductionID: ALL
      inTCKs: ALL

  steps:  # There are 2 steps
  - name: {MOORE_STEP_NAME}  # Ensure some relation to "name" given above, e.g. "Passthrough Sprucing"
    processing_pass: {MOORE_PROCESSING_PASS}  # E.g. "Sprucing24c1", this will form part of the bkk path for sprucing output
    visible: true
    application:
      name: Moore
      version: {MOORE_VERSION}
    data_pkgs: [SprucingConfig.v2XrYpZ]
    options:
      entrypoint: {ENTRY_POINT}  # E.g. SprucingConfig.Spruce24.Sprucing_production_physics_pp_Collision24c1:turbo_spruce_production
      extra_options:
        input_process: Hlt2
        process: {PROCESS} # Spruce for FULL and TURCAL else TurboPass
        input_type : RAW
        input_raw_format : 0.5
        data_type : Upgrade
        simulation : False
        geometry_version: run3/2024.Q1.2-v00.00 # Check this with RTA!
        conditions_version: master
        output_type : ROOT
        compression: ZSTD:1
    input:
    - type: MDF
      visible: true
    output:  # IMPORTANT: some types may only be relevant for Exclusive / Passthrough. This needs a careful check!
    - type: ["B2CC.DST", "B2OC.DST", "BANDQ.DST", "BNOC.DST", "CHARM.DST", "QEE.DST", "RD.DST", "SL.DST"]
      visible: false

  - name: {MERGE_STEP_NAME}  # Ensure some relation to "name" in step above, e.g. "Passthrough sprucing merge"
    processing_pass: {MERGE_PROCESSING_PASS}  # E.g. "Sprucing24c1_merge"
    visible: false
    application:
      name: LHCb
      version: {LHCB_VERSION_FOR_CHOOSEN_MOORE_VERSION}
    data_pkgs: []
    options:
      entrypoint: GaudiConf.mergeDST:dst
      extra_options:
        input_process: Hlt2
        input_type : ROOT
        input_raw_format : 0.5
        data_type : Upgrade
        simulation : False
        geometry_version: run3/2024.Q1.2-v00.00
        conditions_version: master
        output_type : ROOT
        compression: "LZMA:4"
    input:  # Should match the "output" list of the Moore step above!
    - type: ["B2CC.DST", "B2OC.DST", "BANDQ.DST", "BNOC.DST", "CHARM.DST", "QEE.DST", "RD.DST", "SL.DST"]
      visible: false
    output:
    - type: ["B2CC.DST", "B2OC.DST", "BANDQ.DST", "BNOC.DST", "CHARM.DST", "QEE.DST", "RD.DST", "SL.DST"]
      visible: true 
```
