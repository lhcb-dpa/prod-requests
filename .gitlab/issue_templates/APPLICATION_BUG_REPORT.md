# Application bug report

### Summary

[Summarise the bug(s) encountered concisely.]

### Steps or hints to reproduce

[Detail or hint at how one can reproduce the issue - this is very important.]

### Relevant logs and/or screenshots

[Paste any relevant logs - use code blocks (```) to format console output, logs
and code, as it's very hard to read otherwise.]

### Possible fixes

[If you can, link to the line of code that might be responsible for the problem.]

/label ~Bug
/cc @nskidmor
